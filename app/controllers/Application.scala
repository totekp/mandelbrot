package controllers

import play.api._
import play.api.mvc._
import java.util.regex.Pattern
import com.google.appengine.demos.mandelbrot.TileFactory
import com.google.appengine.demos.mandelbrot.PngWriter
import com.google.appengine.demos.mandelbrot.MandelbrotSource
import com.google.appengine.demos.mandelbrot.Palette
import play.cache.Cache
import play.mvc.Http
import play.api.http.Writeable

object Application extends Controller {
  
    /**
   * Expect requests of the form {@code level/x_y.ext}, where {@code
   * level}, {@code x}, and {@code y} are integers and {@code ext} is
   * a file extension.
   */
  private val PATH_INFO_PATTERN = Pattern.compile("^/(\\d+)/(\\d+)_(\\d+)\\..*$")

  /**
   * Maximum amount of time that clients are allowed to cache tiles.
   * This is just an arbitrary amount of time, so we'll just use one
   * year.
   */
  private val MAX_AGE = 365 * 24 * 60 * 60

  /**
   * Store a single {@link Cache} reference for use across requests.
   */
  //using Play Cache
  
  /**
   * This {@link TileFactory} will be used to generate {@link
   * PixelSource} objects for each request.
   */
  private val tileFactory: TileFactory = new TileFactory(new MandelbrotSource(new Palette()));
  
  /**
   * This {@link ImageWriter} will be used to generate the byte stream
   * that we return to the client.
   */
  private val imageWriter = new PngWriter();

  

  /**
   * Look up the request in the distributed cache and, if found, serve
   * the image directly.  If it is not found, call {@ink
   * generateImage} and insert the results into the cache.
   */
  def doGet(name: String) = Action { implicit request =>
    val cacheKey = getCacheKey(request)

    //Logger.info("Retrieving " + cacheKey + " from cache...");
    val start = System.nanoTime();
    val data = Cache.get(cacheKey)
    val image: Array[Byte] = {
      data match {
        case null =>
          Logger.info("Not found, generating...")
          val newImage = generateImage(request)
          Logger.info("Generated. Adding to cache...")
          Cache.set(cacheKey, newImage)
          Logger.info("Added.")
          newImage
        case Array =>
          Logger.info("Found")
          data.asInstanceOf[Array[Byte]]
      }
    }
    
    val expiresTime = System.currentTimeMillis() + MAX_AGE * 1000
    val cacheControl = "max-age=" + MAX_AGE + ", public"

    Ok(image)
      .as(imageWriter.getContentType).withHeaders(
        EXPIRES -> expiresTime.toString,
        CACHE_CONTROL -> cacheControl)
  }

  /**
   * Extract the tile level and coodinates from {@code request} and
   * generate an image that corresponds to the requested tile.
   */
  def generateImage(request: Request[AnyContent])  = {
    val path = request.path.substring("/mandelbrot256_files".length)
	//Logger.debug(path);
    val matcher = PATH_INFO_PATTERN.matcher(path)
    if (!matcher.matches()) {
      throw sys.error("Could not match: " + path)
      //throw PlayException("Could not match: " + path);
    }
    val level = Integer.parseInt(matcher.group(1));
    val tileX = Integer.parseInt(matcher.group(2));
    val tileY = Integer.parseInt(matcher.group(3));

    imageWriter.generateImage(tileFactory.createTile(level, tileX, tileY));
  }

  /**
   * Extract a key from {@code request} suitable for use in a cache.
   * This includes not only the request URI, but also the version
   * identifier of the current application.
   */
  private def getCacheKey(request: Request[AnyContent]) = request.uri
  
}